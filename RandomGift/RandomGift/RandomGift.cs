﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomGift
{
    class RandomGift
    {
        public void Start()
        {
            int Input = int.Parse(MainMenue());          

            switch (Input)
            {
                case 1:
                    SearchClient();
                    break;
                case 2:
                    AddNewClient();
                    break;
                case 3:
                    DeleteClient();
                    break;
                case 4:
                    UpdateClient();
                    break;
                case 5:
                    DisplayClients();
                    break;
                case 6:
                    PickWinner();
                    break;
                default:
                    Console.WriteLine("Invalid input");
                    break;
            };
        }

        public string MainMenue()
        {
            string input;

            Console.WriteLine("\n\n * * * * * * * * MAIN MENU * * * * * * * * ");
            Console.WriteLine("\n\n1. Search for a client " +
                "\n2. Add a new client " +
                "\n3. Delete a client " +
                "\n4. Update a client " +
                "\n5. Display all clients" +
                "\n6. Pick a winner ");

            do
            {
                Console.WriteLine("\nInput a number ( 1 to 6 ): ");
                input = Console.ReadLine();
            } while (string.IsNullOrWhiteSpace(input) || (input != "1" && input != "2" && input != "3" && input != "4" && input != "5" && input != "6"));

            return input;
        }     

        public void SearchClient()
        {
            Console.WriteLine("\n\n* * * * SEARCH A CLIENT * * * *");

            using (LocalDataBaseContainer Data = new LocalDataBaseContainer())
            {
                string input;
                int ID = 0;
                bool isValid;
                int error;
                bool IsExist = false;

                var SearchID = from e in Data.Clients   // Get all ID in database
                               select e.Id;

                var Client = from c in Data.Clients  // Get info from client table
                             select c;

                if (!Client.Any())
                {
                    Console.WriteLine("\n\nThere are any clients in database.");
                }
                else
                {
                    do                  // Ask for input a valid client ID
                    {
                        Console.WriteLine("\nInput an ID :");
                        input = Console.ReadLine();

                        if (int.TryParse(input, out error))
                        {
                            isValid = true;
                            ID = int.Parse(input);
                        }
                        else
                        {
                            isValid = false;
                        }

                        foreach (int i in SearchID) // Research if the input ID exist or nots
                        {
                            if (i == ID)
                            {
                                IsExist = true;
                                break;
                            }
                            else { IsExist = false; }
                        }

                        if (IsExist == false) { Console.WriteLine("ID doesn't exist."); }
                    } while (string.IsNullOrWhiteSpace(input) || isValid == false || IsExist == false);

                    var Getwish = from g in Data.WishList   // Get the wish list of the client
                                  where g.Clients.Id == ID
                                  select g;

                    var GetObject = from o in Data.Objects
                                    select o;

                    Console.WriteLine("\n\n * Result * ");

                    foreach (var g in Getwish)  // Display client information
                    {
                        Console.WriteLine($"\nID CLIENT : {g.Clients.Id}" +
                            $"\nFull name : {g.Clients.FullName}" +
                            $"\nPhone number : {g.Clients.Phone}" +
                            $"\nEmail : {g.Clients.Email}" +
                            $"\nWish List : ");

                        foreach (var o in GetObject) // Display the wish list
                        {
                            if (g.Wish1 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish2 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish3 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish4 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish5 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish6 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish7 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish8 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish9 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish10 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                        }
                    }
                }

                Console.WriteLine("\n\n\nProcess ended.");
            }

            askToBack();
        }

        public void AddNewClient()
        {
            Console.WriteLine("\n\n* * * * ADD A NEW CLIENT * * * *");

            using (LocalDataBaseContainer Data = new LocalDataBaseContainer())
            {
                Data.Database.CreateIfNotExists();

                string FullName;
                string num;
                string Email;
                bool NumisValid;
                bool EmailValid;
                bool wishIsValid = false;
                long error;

                List<string> InpuList = new List<string> { };
                List<int> LWish = new List<int>();
                int nbrWish = 0;
                string input;
                int wish;
                string askWishList;

                #region Ask for new client informations

                do                           // FullName
                {
                    Console.WriteLine("\nFullName :");
                    FullName = Console.ReadLine();

                } while (string.IsNullOrWhiteSpace(FullName));

                do                           // Email
                {
                    Console.WriteLine("Email :");
                    Email = Console.ReadLine();

                    if (Email.Contains("@") && Email.Contains(".") &&
                        !Email.StartsWith("@") && !Email.EndsWith("@") &&
                        !Email.StartsWith(".") && !Email.EndsWith(".") )
                    {
                        EmailValid = true;
                    }
                    else
                    {
                        EmailValid = false;
                        Console.WriteLine("\nInvalid email.");
                    }

                } while (string.IsNullOrWhiteSpace(Email) || EmailValid == false);

                do                           // Phone number
                {
                    Console.WriteLine("Phone :");
                    num = Console.ReadLine();

                    if (long.TryParse(num, out error) && num.Count() == 10)
                    {
                        NumisValid = true;

                    }
                    else
                    {
                        NumisValid = false;
                        Console.WriteLine("\nInvalid phone number. Must be have 10 numbers.");
                    }

                } while (string.IsNullOrWhiteSpace(num) || NumisValid == false);

                #endregion

                #region Ask for wish list informations
                do                              // ask for wish list 
                {
                    Console.WriteLine($"\n\nDo you want to add wish list ? Y/N" +
                        $"\nIf you don't add wish list, the current action has been canceled.");
                    askWishList = Console.ReadLine();

                } while (string.IsNullOrWhiteSpace(askWishList) || (askWishList != "Y" && askWishList != "y" && askWishList != "N" && askWishList != "n"));

                if (askWishList == "Y" || askWishList == "y")
                {
                    Console.WriteLine("\n\n * * Objects list * * ");

                    var ObjList = from o in Data.Objects      
                                  select o;

                    foreach (Objects a in ObjList)  //Display all objects
                    {
                        Console.WriteLine($"\n\nID :{a.Id}\nObject :{a.Name}");
                    }

                    Console.WriteLine("\n\nYou can have maximum 10 wishes." +
                        "\nInput 0 if you haven't wish." +
                        "\n\nEnter an object ID to add a new object to the wish list :");

                    do                          // ask for input object to add in wish list
                    {
                        input = Console.ReadLine();
                        InpuList.Add(input);

                        if (long.TryParse(input, out error) )
                        {
                            wishIsValid = true;
                            wish = int.Parse(input);

                            if (0 <= wish && wish <= 20 && LWish.Count() < 10)
                            {
                                nbrWish += 1;
                                LWish.Add(wish);
                                Console.WriteLine($"\nWish{nbrWish} added.");
                            }
                            else if (wish <  0 || 20 < wish)
                            {
                                wishIsValid = false;
                                Console.WriteLine("\nID doesn't exist.");
                            }
                        }
                        else
                        {
                            wishIsValid = false;
                            Console.WriteLine("\nID must be a number.");
                        }
                    }
                    while (wishIsValid == false || (LWish.Count() < 10 && nbrWish < 10));

                    Data.Clients.Add(new Clients()  // Add in the Data Base
                    {
                        FullName = FullName,
                        Phone = long.Parse(num),
                        Email = Email,
                        WishList = new WishList
                        {
                            Wish1 = LWish[0],
                            Wish2 = LWish[1],
                            Wish3 = LWish[2],
                            Wish4 = LWish[3],
                            Wish5 = LWish[4],
                            Wish6 = LWish[5],
                            Wish7 = LWish[6],
                            Wish8 = LWish[7],
                            Wish9 = LWish[8],
                            Wish10 = LWish[9]
                        }
                    });

                    Data.SaveChanges();

                    var Getwish = from g in Data.WishList   // Get wish list of the new client
                                  where g.Clients.FullName == FullName
                                  select g;

                    var GetObject = from o in Data.Objects  // Get all Object
                                    select o;

                    Console.WriteLine("\n\n * Recapitulation *");

                    foreach (var g in Getwish)  // Display information about new client
                    {
                        Console.WriteLine($"\n\nID CLIENT : {g.Clients.Id}" +
                            $"\nFull name : {g.Clients.FullName}" +
                            $"\nPhone number : {g.Clients.Phone}" +
                            $"\nEmail : {g.Clients.Email}" +
                            $"\nWish List : ");

                        foreach (var o in GetObject)    // Display his wish list 
                        {

                            if (g.Wish1 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish2 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish3 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish4 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish5 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish6 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish7 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish8 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish9 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish10 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                        }
                    }                                      
                    Console.WriteLine($"\n\nThe new client has been added successfully.\nProcess ended.");
                }
                else { Console.WriteLine("\nProcess canceled."); }
                #endregion

                Data.SaveChanges();
            }
            askToBack();
        }

        public void DeleteClient()
        {
            Console.WriteLine("\n\n* * * * DELETE A CLIENT * * * *");

            using (LocalDataBaseContainer Data = new LocalDataBaseContainer())
            {
                Data.Database.CreateIfNotExists();

                string input;
                string confirmation;
                int ID = 0;
                bool isValid;
                int error;
                bool IsExist = false;

                var SearchID = from e in Data.Clients   
                               select e.Id;

                var Client = from c in Data.Clients  // Get info from client table
                             select c;

                if (!Client.Any())
                {
                    Console.WriteLine("\n\nThere are any clients in database.");
                }
                else
                {

                    do                  // Ask for input a valid client ID
                    {
                        Console.WriteLine("\nInput an ID :");
                        input = Console.ReadLine();

                        if (int.TryParse(input, out error))
                        {
                            isValid = true;
                            ID = int.Parse(input);
                        }
                        else
                        {
                            isValid = false;
                            Console.WriteLine("ID must be an number.");
                        }

                        foreach (int i in SearchID) // Research if ID exist or not
                        {
                            if (i == ID)
                            {
                                IsExist = true;
                                break;
                            }
                            else { IsExist = false; }
                        }
                        if (IsExist == false) { Console.WriteLine("ID doesn't exist."); }

                    } while (string.IsNullOrWhiteSpace(input) || isValid == false || IsExist == false);

                    var remov = from r in Data.Clients.Include("WishList")  // Search the client with the input ID
                                where r.Id == ID
                                select r;

                    foreach (Clients c in remov)    // Display found results
                    {
                        Console.WriteLine(
                            $"\n\n * Result *\n\nFull Name :{c.FullName}" +
                            $"\nID : {c.Id}" +
                            $"\nPhone :{c.Phone}" +
                            $"\nEmail :{c.Email} " +
                            $"\n\nAre you sur to delete it ? Y/N ");
                        confirmation = Console.ReadLine();

                        if (confirmation == "Y" || confirmation == "y")    // Ask for confirmation to delete
                        {
                            Data.Clients.Remove(c);
                            Console.WriteLine("\nsuccessfully deleted.\nProcess ended.");
                        }
                    };
                }

                Data.SaveChanges();
            }
            askToBack();
        }

        public void UpdateClient()
        {
            Console.WriteLine("\n\n * * * * UPDATE A CLIENT * * * *");

            using (LocalDataBaseContainer Data = new LocalDataBaseContainer())
            {
                string input;
                int ID = 0;
                bool isValid;
                int error;
                long error2;
                bool IsExist = true;


                string newFullName;
                string newEmail;
                string newPhone;
                string askWishList;
                string wishInput;
                int wish;
                int nbreWish = 0;
                List<string> InputWish = new List<string>();
                List<int> LWish = new List<int>();

                bool EmailValid;
                bool ValidPhone;
                bool WishIsValid;

                #region choose a client

                var Client = from c in Data.Clients  // Get info from client table
                             select c;

                if (!Client.Any())  
                {
                    Console.WriteLine("\n\nThere are any clients in database.");
                }
                else
                {
                    var SearchID = from e in Data.Clients   // Get all ID in database
                                   select e.Id;

                    do                  // Ask for input a valid client ID
                    {
                        Console.WriteLine("\nInput an ID :");
                        input = Console.ReadLine();

                        if (int.TryParse(input, out error))
                        {
                            isValid = true;
                            ID = int.Parse(input);
                        }
                        else
                        {
                            isValid = false;
                        }

                        foreach (int i in SearchID) // Research if the input ID exist or nots
                        {
                            if (i == ID)
                            {
                                IsExist = true;
                                break;
                            }
                            else { IsExist = false; }
                        }

                        if (IsExist == false) { Console.WriteLine("ID doesn't exist."); }
                    } while (string.IsNullOrWhiteSpace(input) || isValid == false || IsExist == false);

                    var Getwish = from g in Data.WishList   // Get the wish list of the client
                                  where g.Clients.Id == ID
                                  select g;

                    var GetObject = from o in Data.Objects  //Get all objects
                                    select o;

                    foreach (var g in Getwish)  // Display client information
                    {

                        Console.WriteLine($"\n\n * Result * " +
                            $"\n\nID CLIENT : {g.Clients.Id}" +
                            $"\n Full name : {g.Clients.FullName}" +
                            $"\n Phone number : {g.Clients.Phone}" +
                            $"\n Email : {g.Clients.Email}" +
                            $"\n Wish List : ");

                        foreach (var o in GetObject) // Display the wish list
                        {
                            if (g.Wish1 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish2 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish3 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish4 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish5 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish6 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish7 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish8 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish9 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish10 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                        }
                    }
                    Console.WriteLine("\n\nPress a touch to confirm.");
                    Console.ReadLine();

                    #endregion

                    Clients pers = (from p in Data.Clients      // Get the client in database
                                    where p.Id == ID
                                    select p).First();

                    WishList Addwish = (from w in Data.WishList     // Get his wish list in database
                                        where w.Clients.Id == ID
                                        select w).First();

                    #region input new informations

                    do                           // new Name
                    {
                        Console.WriteLine("\n\nNew Full Name :");
                        newFullName = Console.ReadLine();

                    } while (string.IsNullOrWhiteSpace(newFullName));

                    do                           // new Email
                    {
                        Console.WriteLine("New Email :");
                        newEmail = Console.ReadLine();

                        if (newEmail.Contains("@") && newEmail.Contains(".") &&
                            !newEmail.StartsWith("@") && !newEmail.EndsWith("@") &&
                            !newEmail.StartsWith(".") && !newEmail.EndsWith("."))
                        {
                            EmailValid = true;
                        }
                        else
                        {
                            EmailValid = false;
                            Console.WriteLine("\nInvalid email.");
                        }

                    } while (string.IsNullOrWhiteSpace(newEmail) || EmailValid == false);


                    do                           // new phone number
                    {
                        Console.WriteLine("New Phone :");
                        newPhone = Console.ReadLine();

                        if (long.TryParse(newPhone, out error2) && newPhone.Count() == 10)
                        {
                            ValidPhone = true;

                        }
                        else
                        {
                            ValidPhone = false;
                            Console.WriteLine("\nInvalid phone number. Must be have 10 numbers.");
                        }

                    } while (string.IsNullOrWhiteSpace(newPhone) || ValidPhone == false);

                    do          // Ask for client if need to update wish list too                     
                    {
                        Console.WriteLine($"\n\nDo you want to update the wish list too ? Y/N");
                        askWishList = Console.ReadLine();

                    } while (string.IsNullOrWhiteSpace(askWishList) || (askWishList != "Y" && askWishList != "y" && askWishList != "N" && askWishList != "n"));

                    if (askWishList == "Y" || askWishList == "y")   // start to update wish list
                    {
                        Console.WriteLine("\n\n * * Objects list * * ");

                        var ObjList = from o in Data.Objects
                                      select o;

                        foreach (Objects a in ObjList)  //Display all objects
                        {
                            Console.WriteLine($"\n\nID :{a.Id}\nObject :{a.Name}");
                        }

                        Console.WriteLine("\n\nEnter an object ID to add in the new wish list :");

                        do                          // ask for input the new wish list
                        {
                            wishInput = Console.ReadLine();
                            InputWish.Add(input);

                            if (int.TryParse(wishInput, out error))
                            {
                                WishIsValid = true;
                                wish = int.Parse(wishInput);
                                if (0 <= wish && wish <= 20 && LWish.Count() < 10)
                                {
                                    nbreWish += 1;
                                    LWish.Add(wish);
                                    Console.WriteLine($"\nWish{nbreWish} added.");
                                }
                                else if (wish < 0 || 20 < wish)
                                {
                                    WishIsValid = false;
                                    Console.WriteLine("\nID doesn't exist.");
                                }
                            }
                            else
                            {
                                WishIsValid = false;
                                Console.WriteLine("\nID must be a number.");
                            }
                        }
                        while (WishIsValid == false || (LWish.Count() < 10 && nbreWish < 10));

                        pers.FullName = newFullName;            // update informations from database
                        pers.Phone = long.Parse(newPhone);
                        pers.Email = newEmail;

                        Addwish.Wish1 = LWish[0];
                        Addwish.Wish2 = LWish[1];
                        Addwish.Wish3 = LWish[2];
                        Addwish.Wish4 = LWish[3];
                        Addwish.Wish5 = LWish[4];
                        Addwish.Wish6 = LWish[5];
                        Addwish.Wish7 = LWish[6];
                        Addwish.Wish8 = LWish[7];
                        Addwish.Wish9 = LWish[8];
                        Addwish.Wish10 = LWish[9];

                    }
                    else
                    {
                        pers.FullName = newFullName;            // update informations from database
                        pers.Phone = long.Parse(newPhone);
                        pers.Email = newEmail;
                    }

                    #endregion

                    Console.WriteLine("\n\n\n * Recapitulation *");

                    foreach (var g in Getwish)  // Display a recapitulation
                    {
                        Console.WriteLine($"\n\nID CLIENT : {g.Clients.Id}" +
                            $"\nNew Full name : {g.Clients.FullName}" +
                            $"\nNew Phone number : {g.Clients.Phone}" +
                            $"\nNew Email : {g.Clients.Email}" +
                            $"\nWish List : ");

                        foreach (var o in GetObject)
                        {
                            if (g.Wish1 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish2 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish3 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish4 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish5 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish6 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish7 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish8 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish9 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish10 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                        }
                    }
                }
            
              
                Data.SaveChanges();
                Console.WriteLine("\n\n\nSuccessfully updated.");
                askToBack();
            }
        }

        public void DisplayClients()
        {
            Console.WriteLine("\n\n* * * * LIST OF ALL CLIENTS * * * *");

            using (LocalDataBaseContainer Data = new LocalDataBaseContainer())
            {
                var Getwish = from g in Data.WishList   // Get info from wish list
                              select g;

                var GetObject = from o in Data.Objects  // Get info from Object table
                                select o;

                var Client = from c in Data.Clients  // Get info from client table
                                select c;

                if (!Client.Any())
                {
                    Console.WriteLine("\n\nThere are any clients in database.");
                }
                else
                {

                    foreach (var g in Getwish)  // search client according to ID wish list
                    {
                        Console.WriteLine($"\n\nID CLIENT : {g.Clients.Id}" +
                            $"\nFull name : {g.Clients.FullName}" +
                            $"\nPhone number : {g.Clients.Phone}" +
                            $"\nEmail : {g.Clients.Email}" +
                            $"\nWish List : ");

                        foreach (var o in GetObject)    // Display wish list 
                        {

                            if (g.Wish1 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish2 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish3 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish4 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish5 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish6 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish7 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish8 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish9 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                            if (g.Wish10 == o.Id) { Console.WriteLine($"\t- {o.Name} , ID Reference : {o.Id}"); }
                        }
                    }
                }
            }

            askToBack();
        }

        public void PickWinner()
        {
            Console.WriteLine("\n\n* * * * PICK A WINNER * * * *");
            
            Random random = new Random();

            using (LocalDataBaseContainer Data = new LocalDataBaseContainer())
            {              
                List<int> IDclient = new List<int>() { };
                List<int> IDwish = new List<int>() { };


                var GetClients = from e in Data.Clients   // Get all existing ID in database
                               select e.Id;

                if (!GetClients.Any())
                {
                    Console.WriteLine("\n\nThere are not clients in database.");
                }
                else
                {
                    foreach (int i in GetClients)  // Add ID in the ID list 
                    {
                        IDclient.Add(i);
                    }

                    int indexID = random.Next(IDclient.Count());    // Random ID client
                    int RandomID = IDclient[indexID];

                    var GetRandomClient = from c in Data.Clients
                                          where c.Id == RandomID
                                          select c;

                    var GetWishList = from w in Data.WishList       // Get the wish list of the random client
                                      where w.Clients.Id == RandomID
                                      select w;

                    var GetObj = from o in Data.Objects
                                 select o;

                    foreach (var w in GetWishList)     // Add all existing wish ID in IDWish (a list)
                    {
                        foreach (var o in GetObj)
                        {
                            if (w.Wish1 == o.Id) { IDwish.Add(w.Wish1); }
                            if (w.Wish2 == o.Id) { IDwish.Add(w.Wish2); }
                            if (w.Wish3 == o.Id) { IDwish.Add(w.Wish3); }
                            if (w.Wish4 == o.Id) { IDwish.Add(w.Wish4); }
                            if (w.Wish5 == o.Id) { IDwish.Add(w.Wish5); }
                            if (w.Wish6 == o.Id) { IDwish.Add(w.Wish6); }
                            if (w.Wish7 == o.Id) { IDwish.Add(w.Wish7); }
                            if (w.Wish8 == o.Id) { IDwish.Add(w.Wish8); }
                            if (w.Wish9 == o.Id) { IDwish.Add(w.Wish9); }
                            if (w.Wish10 == o.Id) { IDwish.Add(w.Wish10); }

                        }
                    }

                    int indexWish = random.Next(IDwish.Count());    // Random wish 
                    int RandomWish = IDwish[indexWish];

                    var RandomObjt = from o in Data.Objects     // Get info about the wish
                                     where o.Id == RandomWish
                                     select o;

                    Console.WriteLine("\n\n * Winner * ");  // output the winner
                    foreach (Clients c in GetRandomClient)
                    {
                        Console.WriteLine($"\nFullName : {c.FullName}" +
                            $"\nPhone : {c.Phone}" +
                            $"\nEmail : {c.Email}");
                    }

                    foreach (Objects o in RandomObjt)
                    {
                        Console.WriteLine($"\nWish : {o.Name}" +
                            $"\nValue : {o.Value} euros" +
                            $"\nID Reference : {o.Id}");
                    }
                }

                
                
                Data.SaveChanges();
            }                      
            askToBack();
        }

        public void askToBack()
        {
            string toBack;
            Console.WriteLine("\n\n\n\nPress Z to back \n");
            toBack = Console.ReadLine();
            if (toBack == "z" || toBack == "Z")
            {
                Start();
            }
        }

    }
}
