
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 01/04/2019 20:17:44
-- Generated from EDMX file: C:\Users\cheny\Desktop\282940_2NET_Paris_RandomGift\RandomGift\LocalDataBase.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [localDatabase];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ClientsWishList]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WishList] DROP CONSTRAINT [FK_ClientsWishList];
GO
IF OBJECT_ID(N'[dbo].[FK_WishListObjects]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WishList] DROP CONSTRAINT [FK_WishListObjects];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Clients]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Clients];
GO
IF OBJECT_ID(N'[dbo].[WishList]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WishList];
GO
IF OBJECT_ID(N'[dbo].[Objects]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Objects];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Clients'
CREATE TABLE [dbo].[Clients] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FullName] nvarchar(max)  NOT NULL,
    [Phone] bigint  NOT NULL,
    [Email] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'WishList'
CREATE TABLE [dbo].[WishList] (
    [Id] int  NOT NULL,
    [Wish1] int  NOT NULL,
    [Wish2] int  NOT NULL,
    [Wish3] int  NOT NULL,
    [Wish4] int  NOT NULL,
    [Wish5] int  NOT NULL,
    [Wish6] int  NOT NULL,
    [Wish7] int  NOT NULL,
    [Wish8] int  NOT NULL,
    [Wish9] int  NOT NULL,
    [Wish10] int  NOT NULL
);
GO

-- Creating table 'Objects'
CREATE TABLE [dbo].[Objects] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Value] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Clients'
ALTER TABLE [dbo].[Clients]
ADD CONSTRAINT [PK_Clients]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'WishList'
ALTER TABLE [dbo].[WishList]
ADD CONSTRAINT [PK_WishList]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Objects'
ALTER TABLE [dbo].[Objects]
ADD CONSTRAINT [PK_Objects]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Id] in table 'WishList'
ALTER TABLE [dbo].[WishList]
ADD CONSTRAINT [FK_ClientsWishList]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Clients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Wish1] in table 'WishList'
ALTER TABLE [dbo].[WishList]
ADD CONSTRAINT [FK_WishListObjects]
    FOREIGN KEY ([Wish1])
    REFERENCES [dbo].[Objects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WishListObjects'
CREATE INDEX [IX_FK_WishListObjects]
ON [dbo].[WishList]
    ([Wish1]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------