﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//CampusID 282940

namespace RandomGift
{
    class Program
    {
        static void Main(string[] args)
        {
            
            #region Add a object list

            // this sessionion is used to initialize the objects in DataBase

            using (LocalDataBaseContainer DataBase = new LocalDataBaseContainer())
            {                               
                Objects creme1 = new Objects { Name = "Creme de visage", Value = 20 };
                Objects creme2 = new Objects { Name = "Creme de main ", Value = 20 };
                Objects creme3 = new Objects { Name = "Creme de corps", Value = 20 };
                Objects nettoyant = new Objects { Name = "Nettoyant visage", Value = 40 };
                Objects shamppoing = new Objects { Name = "Shampoing antipelliculaire", Value  = 10 };

                Objects bague = new Objects { Name = "Bague", Value = 150 };
                Objects collier = new Objects { Name = "Collier de perle", Value = 100 };
                Objects bracelet = new Objects { Name = "bracelet BFF", Value = 105 };
                Objects sacoche = new Objects { Name = "Sacoche", Value = 50 };
                Objects sacMain = new Objects { Name = "Sac à main ", Value = 60 };

                Objects batterie = new Objects { Name = "Batterie de secours ", Value = 49 };
                Objects chargeur1 = new Objects { Name = "Cable iphone", Value = 30 };
                Objects chargeur2 = new Objects { Name = "Cable nokia", Value = 10 };
                Objects coque = new Objects { Name = "Coque iphone ", Value = 20 };
                Objects film = new Objects { Name = "Film  ", Value = 10 };

                Objects Veste = new Objects { Name = "Creme corps ", Value = 30 };
                Objects pantalon = new Objects { Name = "Jean bleu Femme", Value = 20 };
                Objects manteau = new Objects { Name = "Manteau femme et homme", Value = 160 };
                Objects chaussure = new Objects { Name = " Chaussure nike", Value = 110 };
                Objects gants = new Objects { Name = "Gants ultra chaud", Value = 10 };

                if (!DataBase.Objects.Any())
                {
                    DataBase.Objects.Add(creme1);
                    DataBase.Objects.Add(creme2);
                    DataBase.Objects.Add(creme3);
                    DataBase.Objects.Add(nettoyant);
                    DataBase.Objects.Add(shamppoing);

                    DataBase.Objects.Add(bague);
                    DataBase.Objects.Add(collier);
                    DataBase.Objects.Add(bracelet);
                    DataBase.Objects.Add(sacMain);
                    DataBase.Objects.Add(sacoche);

                    DataBase.Objects.Add(batterie);
                    DataBase.Objects.Add(chargeur1);
                    DataBase.Objects.Add(chargeur2);
                    DataBase.Objects.Add(coque);
                    DataBase.Objects.Add(film);

                    DataBase.Objects.Add(Veste);
                    DataBase.Objects.Add(pantalon);
                    DataBase.Objects.Add(manteau);
                    DataBase.Objects.Add(chaussure);
                    DataBase.Objects.Add(gants);
                }
                                
                DataBase.SaveChanges();
            }
            #endregion

            RandomGift db = new RandomGift();
            db.Start();

            Console.ReadLine();
        }
    }
}
